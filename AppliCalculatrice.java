import javafx.application.Application;
import javafx.application.Platform;
// import javafx.beans.binding.Bindings;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


public class AppliCalculatrice extends Application {
    private Label resultat;
    private TextField tf1;
    private TextField tf2;
    private Calculatrice calculatrice;
    @Override
    public void init() {
        this.tf1 = new TextField();
        this.tf2 = new TextField();
        this.resultat = new Label("Résultat : ");
        this.calculatrice = new Calculatrice();
    }
    @Override
    public void start(Stage stage) throws Exception{
        VBox root = new VBox();
        this.ajouteLabel(root);
        this.ajouteBoutons(root);
        this.ajouteResultat(root);
        Scene scene = new Scene(root);
        stage.setTitle("Calculette");
        stage.setScene(scene);
        stage.show();
    }
    public void effaceTF() {
        this.tf1.setText("");
        this.tf2.setText("");
    }
    public Double getTfNombre1() throws NumberFormatException {
        return Double.parseDouble(tf1.getText());
    }
    public Double getTfNombre2() throws NumberFormatException {
        return Double.parseDouble(tf2.getText());
    }
    public void majResultat() {
        this.resultat.setText("Résultat : " + String.valueOf(this.calculatrice.getResultat()));
    }
    private void ajouteBoutons(Pane root) {
        HBox hbButtons = new HBox(3);
        hbButtons.setPadding(new Insets(10,10,10,10));
        Button buttonAdditionner = new Button("+");
        Button buttonSoustraire = new Button("-");
        buttonAdditionner.setOnAction(new ControleurBoutonAddition(this.calculatrice, this));
        buttonSoustraire.setOnAction(new ControleurBoutonSoustraction(this.calculatrice, this));
        hbButtons.getChildren().addAll(buttonAdditionner, buttonSoustraire);
        hbButtons.setAlignment(Pos.CENTER);
        root.getChildren().add(hbButtons);
    }
    private void ajouteLabel(Pane root) {
        HBox hbChiffres = new HBox(20);
        hbChiffres.setPadding(new Insets(10, 10, 0, 10));
        hbChiffres.setAlignment(Pos.CENTER);    
        Label labelChiffre1 = new Label("");
        Label labelChiffre2 = new Label("");
        hbChiffres.getChildren().add(new HBox(labelChiffre1, this.tf1));
        hbChiffres.getChildren().add(new HBox(labelChiffre2, this.tf2));
        this.tf1.setOnKeyReleased(new ControleurTouche1(this.calculatrice, this));
        this.tf2.setOnKeyReleased(new ControleurTouche2(this.calculatrice, this));
        root.getChildren().add(hbChiffres);
    }
    private void ajouteResultat(Pane root) { 
        HBox resultat = new HBox();
        resultat.setAlignment(Pos.CENTER);
        resultat.getChildren().add(this.resultat);
        root.getChildren().add(resultat);    
    }
    public void quitte() {
        Platform.exit();
    }
    public static void main(String [] args) {
        launch(args);
    }
}
