public class Calculatrice {
    private Double nombre1;
    private Double nombre2;
    private Double resultat;
    public Calculatrice() {
        this.resultat = 0.;
    }
    public Double getNombre1() {
        return this.nombre1;
    }
    public Double getNombre2() {
        return this.nombre2;
    }
    public void setNombre1(Double nombre1){
        this.nombre1 = nombre1;
    }
    public void setNombre2(Double nombre2) {
        this.nombre2 = nombre2;
    }
    public void additionner() {
        this.resultat = this.nombre1 + this.nombre2;
    }
    public void soustraire() {
        this.resultat = this.nombre1 - this.nombre2;
    }
    public Double getResultat() {
        return this.resultat;
    }
}