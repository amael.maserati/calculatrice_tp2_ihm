import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class ControleurBoutonSoustraction implements EventHandler<ActionEvent> {
    private AppliCalculatrice application;
    private Calculatrice calculatrice;
    public ControleurBoutonSoustraction(Calculatrice calculatrice, AppliCalculatrice application) {
        this.calculatrice = calculatrice;
        this.application = application;
    }
    @Override
    public void handle(ActionEvent e) {
        double value1;
        double value2;
        try {
            value1 = this.application.getTfNombre1();
            value2 = this.application.getTfNombre2();
            this.calculatrice.setNombre1(value1);
            this.calculatrice.setNombre2(value2);
            this.calculatrice.soustraire();
            this.application.majResultat();
        } catch (NumberFormatException exp) {
            this.application.effaceTF();
        }
    }
}
