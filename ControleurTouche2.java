import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class ControleurTouche2 implements EventHandler<KeyEvent>{
    private Calculatrice calculatrice;
    private AppliCalculatrice appli;
    public ControleurTouche2(Calculatrice calculatrice, AppliCalculatrice appli) {
        this.calculatrice = calculatrice;
        this.appli = appli;
    }
    @Override
    public void handle(KeyEvent e) {
        if (e.getCode().equals(KeyCode.ENTER)) {
            double value;
            try {
                value = this.appli.getTfNombre2();
                this.calculatrice.setNombre2(value);
                this.appli.majResultat();
            } catch (NumberFormatException exp) {
                this.appli.effaceTF();
            }
        }
    }
}
